// wintab32_.cpp : wintab32_のエントリポイント
//

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
#include <windows.h>
#ifdef _DEBUG
#include <intrin.h>  
  
#pragma intrinsic(_ReturnAddress)  
#endif

//#define WINTABSIDELOAD
//#define WINEPKBUTTUNSFIX
//#define EXTLIMIT
//#define UNITZERO

#define _WINEDEBUG
#ifdef _WINEDEBUG

FILE *fp = NULL;
void MyOutputDebugStringA(LPCSTR s)
{
  //FILE *fp = fopen("dbg.txt", "a");
  if(fp==NULL)return;
  fprintf(fp, s);
  fprintf(fp, "\n");
  //fclose(fp);
}
#elif
 #define MyOutputDebugStringA OutputDebugStringA
#endif

DECLARE_HANDLE(HCTX);		/* context handle */

//FARPROC p_WTInfoA;
typedef unsigned int (__stdcall *D_WTInfoA)(unsigned int, unsigned int, LPVOID);
D_WTInfoA p_WTInfoA;
//FARPROC p_WTOpenA;
typedef HCTX (__stdcall *D_WTOpenA)(HWND, LPVOID, BOOL);
D_WTOpenA p_WTOpenA;
//FARPROC p_WTClose;
typedef BOOL (__stdcall *D_WTClose)(HCTX);
D_WTClose p_WTClose;
//FARPROC p_WTPacketsGet;
typedef int (__stdcall *D_WTPacketsGet)(HCTX, int, LPVOID);
D_WTPacketsGet p_WTPacketsGet;
//FARPROC p_WTPacket;
typedef BOOL (__stdcall *D_WTPacket)(HCTX, unsigned int, LPVOID);
D_WTPacket p_WTPacket;
FARPROC p_WTEnable;
//FARPROC p_WTOverlap;
typedef BOOL (__stdcall *D_WTOverlap)(HCTX, BOOL);
D_WTOverlap p_WTOverlap;
FARPROC p_WTConfig;
//FARPROC p_WTGetA;
typedef int (__stdcall *D_WTGetA)(HCTX, LPVOID);
D_WTGetA p_WTGetA;
FARPROC p_WTSetA;
FARPROC p_WTExtGet;
FARPROC p_WTExtSet;
FARPROC p_WTSave;
FARPROC p_WTRestore;
//FARPROC p_WTPacketsPeek;
FARPROC p_WTDataGet;
FARPROC p_WTDataPeek;
//FARPROC p_WTQueueSizeGet;
typedef int (__stdcall *D_WTQueueSizeGet)(HCTX);
D_WTQueueSizeGet p_WTQueueSizeGet;
//FARPROC p_WTQueueSizeSet;
typedef BOOL (__stdcall *D_WTQueueSizeSet)(HCTX, int);
D_WTQueueSizeSet p_WTQueueSizeSet;
FARPROC p_WTMgrOpen;
FARPROC p_WTMgrClose;
FARPROC p_WTMgrContextEnum;
FARPROC p_WTMgrContextOwner;
FARPROC p_WTMgrDefContext;
FARPROC p_WTMgrDeviceConfig;
FARPROC p_WTMgrExt;
FARPROC p_WTMgrCsrEnable;
FARPROC p_WTMgrCsrButtonMap;
FARPROC p_WTMgrCsrPressureBtnMarks;
FARPROC p_WTMgrCsrPressureResponse;
FARPROC p_WTMgrCsrExt;
FARPROC p_WTQueuePacketsEx;
FARPROC p_WTMgrCsrPressureBtnMarksEx;
FARPROC p_WTMgrConfigReplaceExA;
FARPROC p_WTMgrPacketHookExA;
FARPROC p_WTMgrPacketUnhook;
FARPROC p_WTMgrPacketHookNext;
FARPROC p_WTMgrDefContextEx;
//FARPROC p_WTInfoW;
typedef unsigned int (__stdcall *D_WTInfoW)(unsigned int, unsigned int, LPVOID);
D_WTInfoW p_WTInfoW;
//FARPROC p_WTOpenW;
typedef HCTX (__stdcall *D_WTOpenW)(HWND, LPVOID, BOOL);
D_WTOpenW p_WTOpenW;
//FARPROC p_WTGetW;
typedef BOOL (__stdcall *D_WTGetW)(HCTX, LPVOID);
D_WTGetW p_WTGetW;
FARPROC p_WTSetW;
FARPROC p_WTMgrConfigReplaceExW;
FARPROC p_WTMgrPacketHookExW;

typedef int (__stdcall *D_WTPacketsPeek)(HCTX, int, LPVOID);
D_WTPacketsPeek p_WTPacketsPeek;

HINSTANCE h_original;

BOOL APIENTRY DllMain( HANDLE hModule, 
                    DWORD  ul_reason_for_call, 
                    LPVOID lpReserved
                  )
{
 switch( ul_reason_for_call )
 {
 case DLL_PROCESS_ATTACH:
     h_original = LoadLibrary(
#ifdef WINTABSIDELOAD
        "WINTAB32.dll"
#else
        "WINTAB32_.dll"
#endif
      );
     if ( h_original == NULL )
         return FALSE;
       //p_WTInfoA = GetProcAddress( h_original, "WTInfoA" );
       p_WTInfoA = (D_WTInfoA)GetProcAddress( h_original, "WTInfoA" );
       //p_WTOpenA = GetProcAddress( h_original, "WTOpenA" );
       p_WTOpenA = (D_WTOpenA)GetProcAddress( h_original, "WTOpenA" );
       //p_WTClose = GetProcAddress( h_original, "WTClose" );
       p_WTClose = (D_WTClose)GetProcAddress( h_original, "WTClose" );
       p_WTPacketsGet = (D_WTPacketsGet)GetProcAddress( h_original, "WTPacketsGet" );
       //p_WTPacket = GetProcAddress( h_original, "WTPacket" );
       p_WTPacket = (D_WTPacket)GetProcAddress( h_original, "WTPacket" );
       p_WTEnable = GetProcAddress( h_original, "WTEnable" );
       //p_WTOverlap = GetProcAddress( h_original, "WTOverlap" );
       p_WTOverlap = (D_WTOverlap)GetProcAddress( h_original, "WTOverlap" );
       p_WTConfig = GetProcAddress( h_original, "WTConfig" );
       //p_WTGetA = GetProcAddress( h_original, "WTGetA" );
       p_WTGetA = (D_WTGetA)GetProcAddress( h_original, "WTGetA" );
       p_WTSetA = GetProcAddress( h_original, "WTSetA" );
       p_WTExtGet = GetProcAddress( h_original, "WTExtGet" );
       p_WTExtSet = GetProcAddress( h_original, "WTExtSet" );
       p_WTSave = GetProcAddress( h_original, "WTSave" );
       p_WTRestore = GetProcAddress( h_original, "WTRestore" );
       p_WTPacketsPeek = (D_WTPacketsPeek)GetProcAddress( h_original, "WTPacketsPeek" );
       p_WTDataGet = GetProcAddress( h_original, "WTDataGet" );
       p_WTDataPeek = GetProcAddress( h_original, "WTDataPeek" );
       p_WTQueueSizeGet = (D_WTQueueSizeGet)GetProcAddress( h_original, "WTQueueSizeGet" );
       //p_WTQueueSizeSet = GetProcAddress( h_original, "WTQueueSizeSet" );
       p_WTQueueSizeSet = (D_WTQueueSizeSet)GetProcAddress( h_original, "WTQueueSizeSet" );
       p_WTMgrOpen = GetProcAddress( h_original, "WTMgrOpen" );
       p_WTMgrClose = GetProcAddress( h_original, "WTMgrClose" );
       p_WTMgrContextEnum = GetProcAddress( h_original, "WTMgrContextEnum" );
       p_WTMgrContextOwner = GetProcAddress( h_original, "WTMgrContextOwner" );
       p_WTMgrDefContext = GetProcAddress( h_original, "WTMgrDefContext" );
       p_WTMgrDeviceConfig = GetProcAddress( h_original, "WTMgrDeviceConfig" );
       p_WTMgrExt = GetProcAddress( h_original, "WTMgrExt" );
       p_WTMgrCsrEnable = GetProcAddress( h_original, "WTMgrCsrEnable" );
       p_WTMgrCsrButtonMap = GetProcAddress( h_original, "WTMgrCsrButtonMap" );
       p_WTMgrCsrPressureBtnMarks = GetProcAddress( h_original, "WTMgrCsrPressureBtnMarks" );
       p_WTMgrCsrPressureResponse = GetProcAddress( h_original, "WTMgrCsrPressureResponse" );
       p_WTMgrCsrExt = GetProcAddress( h_original, "WTMgrCsrExt" );
       p_WTQueuePacketsEx = GetProcAddress( h_original, "WTQueuePacketsEx" );
       p_WTMgrCsrPressureBtnMarksEx = GetProcAddress( h_original, "WTMgrCsrPressureBtnMarksEx" );
       p_WTMgrConfigReplaceExA = GetProcAddress( h_original, "WTMgrConfigReplaceExA" );
       p_WTMgrPacketHookExA = GetProcAddress( h_original, "WTMgrPacketHookExA" );
       p_WTMgrPacketUnhook = GetProcAddress( h_original, "WTMgrPacketUnhook" );
       p_WTMgrPacketHookNext = GetProcAddress( h_original, "WTMgrPacketHookNext" );
       p_WTMgrDefContextEx = GetProcAddress( h_original, "WTMgrDefContextEx" );
       p_WTInfoW = (D_WTInfoW)GetProcAddress( h_original, "WTInfoW" );
       p_WTOpenW = (D_WTOpenW)GetProcAddress( h_original, "WTOpenW" );
       p_WTGetW = (D_WTGetW)GetProcAddress( h_original, "WTGetW" );
       p_WTSetW = GetProcAddress( h_original, "WTSetW" );
       p_WTMgrConfigReplaceExW = GetProcAddress( h_original, "WTMgrConfigReplaceExW" );
       p_WTMgrPacketHookExW = GetProcAddress( h_original, "WTMgrPacketHookExW" );
#ifdef _WINEDEBUG
  fp = fopen("dbg.txt", "w");
#endif
     break;
 case DLL_THREAD_ATTACH:
     break;
 case DLL_THREAD_DETACH:
     break;
 case DLL_PROCESS_DETACH:
     FreeLibrary( h_original );
#ifdef _WINEDEBUG
  if(fp!=NULL)
  {
    fclose(fp);
    fp=NULL;
  }
#endif
     break;
 default:
     break;
 }
 return TRUE;
 }


//EXTERN_C int _fltused = 0;
typedef DWORD FIX32;				/* fixed-point arithmetic type */
typedef DWORD WTPKT;			/* packet mask */
	#define FRAC(x)		LOWORD(x)
#define FIX_DOUBLE(x) ((double)(INT(x))+((double)FRAC(x)/65536))
#define LCNAMELEN		40
#define LC_NAMELEN	40
typedef struct tagLOGCONTEXTA {
	char	lcName[LCNAMELEN];
	UINT	lcOptions;
	UINT	lcStatus;
	UINT	lcLocks;
	UINT	lcMsgBase;
	UINT	lcDevice;
	UINT	lcPktRate;
	WTPKT	lcPktData;
	WTPKT	lcPktMode;
	WTPKT	lcMoveMask;
	DWORD	lcBtnDnMask;
	DWORD	lcBtnUpMask;
	LONG	lcInOrgX;
	LONG	lcInOrgY;
	LONG	lcInOrgZ;
	LONG	lcInExtX;
	LONG	lcInExtY;
	LONG	lcInExtZ;
	LONG	lcOutOrgX;
	LONG	lcOutOrgY;
	LONG	lcOutOrgZ;
	LONG	lcOutExtX;
	LONG	lcOutExtY;
	LONG	lcOutExtZ;
	FIX32	lcSensX;
	FIX32	lcSensY;
	FIX32	lcSensZ;
	BOOL	lcSysMode;
	int	lcSysOrgX;
	int	lcSysOrgY;
	int	lcSysExtX;
	int	lcSysExtY;
	FIX32	lcSysSensX;
	FIX32	lcSysSensY;
} LOGCONTEXTA, *PLOGCONTEXTA, NEAR *NPLOGCONTEXTA, FAR *LPLOGCONTEXTA;

typedef struct tagLOGCONTEXTW {
	WCHAR	lcName[LCNAMELEN];
	UINT	lcOptions;
	UINT	lcStatus;
	UINT	lcLocks;
	UINT	lcMsgBase;
	UINT	lcDevice;
	UINT	lcPktRate;
	WTPKT	lcPktData;
	WTPKT	lcPktMode;
	WTPKT	lcMoveMask;
	DWORD	lcBtnDnMask;
	DWORD	lcBtnUpMask;
	LONG	lcInOrgX;
	LONG	lcInOrgY;
	LONG	lcInOrgZ;
	LONG	lcInExtX;
	LONG	lcInExtY;
	LONG	lcInExtZ;
	LONG	lcOutOrgX;
	LONG	lcOutOrgY;
	LONG	lcOutOrgZ;
	LONG	lcOutExtX;
	LONG	lcOutExtY;
	LONG	lcOutExtZ;
	FIX32	lcSensX;
	FIX32	lcSensY;
	FIX32	lcSensZ;
	BOOL	lcSysMode;
	int	lcSysOrgX;
	int	lcSysOrgY;
	int	lcSysExtX;
	int	lcSysExtY;
	FIX32	lcSysSensX;
	FIX32	lcSysSensY;
} LOGCONTEXTW, *PLOGCONTEXTW, NEAR *NPLOGCONTEXTW, FAR *LPLOGCONTEXTW;

	#define PK_CONTEXT				0x0001	/* reporting context */
	#define PK_STATUS					0x0002	/* status bits */
	#define PK_TIME					0x0004	/* time stamp */
	#define PK_CHANGED				0x0008	/* change bit vector */
	#define PK_SERIAL_NUMBER		0x0010	/* packet serial number */
	#define PK_CURSOR					0x0020	/* reporting cursor */
	#define PK_BUTTONS				0x0040	/* button information */
	#define PK_X						0x0080	/* x axis */
	#define PK_Y						0x0100	/* y axis */
	#define PK_Z						0x0200	/* z axis */
	#define PK_NORMAL_PRESSURE		0x0400	/* normal or tip pressure */
	#define PK_TANGENT_PRESSURE	0x0800	/* tangential or barrel pressure */
	#define PK_ORIENTATION			0x1000	/* orientation info: tilts */
	#define PK_ROTATION				0x2000	/* rotation info; 1.1 */

  typedef struct tagAXIS
  {
    LONG    axMin;
    LONG    axMax;
    UINT    axUnits;
    FIX32   axResolution;
  } AXIS, *PAXIS, NEAR *NPAXIS, FAR *LPAXIS;

  #define TU_NONE         0
  #define TU_INCHES       1
  #define TU_CENTIMETERS  2
  #define TU_CIRCLE       3
  
#define FIX_DOUBLE(x)   ((double)(INT(x))+((double)FRAC(x)/65536))   


const char *GetAxUnits(UINT axUnits)
{
  switch(axUnits)
  {
    case 0:
      return "TU_NONE(0)";
    case 1:
      return "TU_INCHES(1)";
    case 2:
      return "TU_CENTIMETERS(2)";
    case 3:
      return "TU_CIRCLE(3)";
  }
  return "Unknown";
}
	
//__declspec( naked ) void d_WTInfoA() { _asm{ jmp p_WTInfoA } }

UINT WINAPI WTInfoA(UINT wCategory, UINT nIndex, LPVOID lpOutput)
{
  BOOL ret = p_WTInfoA(wCategory, nIndex, lpOutput);
  char *buf = (char *)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 1024);
  sprintf(buf, "WTInfoA: wCategory=%u, nIndex=%u, lpOutput=0x%08X, ret=%s\n", wCategory, nIndex, lpOutput, ret?"Success":"Fail");
  MyOutputDebugStringA(buf);
  
  if(ret)
  {
  if(wCategory==0 && nIndex==0)
  {
    MyOutputDebugStringA(ret?"  WinTAB OK!":"  WinTAB fail...\n");
  }
  else if((wCategory==3/*WTI_DEFCONTEXT*/ || wCategory==4/*WTI_DEFSYSCTX*/) && nIndex==0)
  {
    MyOutputDebugStringA((wCategory==3)?"   cat3 WTI_DEFCONTEXT (direct - not cursor)":"   cat4 WTI_DEFSYSCTX (with cursor)");
  LPLOGCONTEXTA lpCon = (LPLOGCONTEXTA)lpOutput;
  #ifdef EXTLIMIT
    if(lpCon->lcInExtX>0xffff)
    {
      sprintf(buf, "     lcInExtX>0xffff - %d->0xffff\n", lpCon->lcInExtX);
      MyOutputDebugStringA(buf);
      lpCon->lcInExtX=0xffff;
    }
    if(lpCon->lcInExtY>0xffff)
    {
      sprintf(buf, "     lcInExtY>0xffff - %d->0xffff\n", lpCon->lcInExtY);
      MyOutputDebugStringA(buf);
      lpCon->lcInExtY=0xffff;
    }
    if(lpCon->lcOutExtX>0xffff)
    {
      sprintf(buf, "     lcOutExtX>0xffff - %d->0xffff\n", lpCon->lcOutExtX);
      MyOutputDebugStringA(buf);
      lpCon->lcOutExtX=0xffff;
    }
    if(lpCon->lcOutExtY>0xffff)
    {
      sprintf(buf, "     lcOutExtY>0xffff - %d->0xffff\n", lpCon->lcOutExtY);
      MyOutputDebugStringA(buf);
      lpCon->lcOutExtY=0xffff;
    }
  #endif //EXTLIMIT
  sprintf(buf, "   lcName=%s\n   lcOptions=%u\n   lcStatus=%u\n   lcLocks=%u\n   lcMsgBase=%u\n   lcDevice=%u\n   lcPktRate=%u\n   lcPktData=0x%08X\n   lcPktMode=0x%08X\n   lcMoveMask=0x%08X\n   lcBtnDnMask=0x%08X\n   lcBtnUpMask=0x%08X\n   lcInOrgX=%d\n   lcInOrgY=%d\n   lcInOrgZ=%d\n   lcInExtX=%d\n   lcInExtY=%d\n   lcInExtZ=%d\n   lcOutOrgX=%d\n   lcOutOrgY=%d\n   lcOutOrgZ=%d\n   lcOutExtX=%d\n   lcOutExtY=%d\n   lcOutExtZ=%d\n   lcSensX=%f\n   lcSensY=%f\n   lcSensZ=%f\n   lcSysMode=%s\n   lcSysOrgX=%d\n   lcSysOrgY=%d\n   lcSysExtX=%d\n   lcSysExtY=%d\n   lcSysSensX=%f\n   lcSysSensY=%f\n", lpCon->lcName, lpCon->lcOptions, lpCon->lcStatus, lpCon->lcLocks, lpCon->lcMsgBase, lpCon->lcDevice, lpCon->lcPktRate, lpCon->lcPktData, lpCon->lcPktMode, lpCon->lcMoveMask, lpCon->lcBtnDnMask, lpCon->lcBtnUpMask, lpCon->lcInOrgX, lpCon->lcInOrgY, lpCon->lcInOrgZ, lpCon->lcInExtX, lpCon->lcInExtY, lpCon->lcInExtZ, lpCon->lcOutOrgX, lpCon->lcOutOrgY, lpCon->lcOutOrgZ, lpCon->lcOutExtX, lpCon->lcOutExtY, lpCon->lcOutExtZ, FIX_DOUBLE(lpCon->lcSensX), FIX_DOUBLE(lpCon->lcSensY), FIX_DOUBLE(lpCon->lcSensZ), (lpCon->lcSysMode)?"TRUE":"FALSE", lpCon->lcSysOrgX, lpCon->lcSysOrgY, lpCon->lcSysExtX, lpCon->lcSysExtY, FIX_DOUBLE(lpCon->lcSysSensX), FIX_DOUBLE(lpCon->lcSysSensY));
  MyOutputDebugStringA(buf);
  }
  else if(wCategory==100/*WTI_DEVICES*/ && nIndex==15/*DVC_NPRESSURE（筆圧値の最大、最小）*/)
  {
  LPAXIS lpPressAxis = (LPAXIS)lpOutput;

  double res = FIX_DOUBLE(lpPressAxis->axResolution); //for R6002 float point support not loaded error

  sprintf(buf, "       WTI_DEVICES:DVC_NPRESSURE: axMax=%d, axMin=%d, axUnits=%s, axResolution=%f\n", lpPressAxis->axMax, lpPressAxis->axMin, GetAxUnits(lpPressAxis->axUnits), res);
  MyOutputDebugStringA(buf);
  #ifdef UNITZERO
  if(lpPressAxis->axUnits!=0)
  {
  sprintf(buf, "       axUnits = %s->0\n", GetAxUnits(lpPressAxis->axUnits));
  MyOutputDebugStringA(buf);
    lpPressAxis->axUnits = 0;
  }
  #endif //UNITZERO
  }
  }
  HeapFree(GetProcessHeap(), 0, buf);
  return ret;
}

	#define CXO_SYSTEM		0x0001
//__declspec( naked ) void d_WTOpenA() { _asm{ jmp p_WTOpenA } }

DWORD g_PktDataStore[10] = {0}; // 本当はHCTXがQWORDになるので駄目。64ビット版はQWORDにする必要あり  {hCtx1, lpPktData1, hCtx2, lpPktData2, hCtx3, lpPktData3...} 

void SetPktDataStore(HCTX hCtx, DWORD lpPktData)
{
  if(hCtx==0)return;
  DWORD dwhCtx = (DWORD)hCtx;
  for(int i=0;i<5;i++)
  {
    if(g_PktDataStore[i*2]==dwhCtx)
    {
      g_PktDataStore[i*2+1]=lpPktData;
      return;
    }
  }
  // not found. New entry
  //search blank slot
  for(int i=0;i<5;i++)
  {
    if(g_PktDataStore[i*2]==0)
    {
      // empty slot here!
      g_PktDataStore[i*2]=dwhCtx;
      g_PktDataStore[i*2+1]=lpPktData;
      return;
    }
  }
  char *buf = (char *)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 1024);
  sprintf(buf, "SetPktDataStore: Failed(no slot) hCtx=0x%08X, lpPktData=0x%08X\n", hCtx, lpPktData);
  MyOutputDebugStringA(buf);
  HeapFree(GetProcessHeap(), 0, buf);
}
void ClosePktDataStore(HCTX hCtx)
{
  if(hCtx==0)return;
  DWORD dwhCtx = (DWORD)hCtx;
  for(int i=0;i<5;i++)
  {
    if(g_PktDataStore[i*2]==dwhCtx)
    {
      g_PktDataStore[i*2]=0;
      g_PktDataStore[i*2+1]=0;
      return;
    }
  }
}
DWORD GetPktDataStore(HCTX hCtx)
{
  if(hCtx==0)return 0;
  DWORD dwhCtx = (DWORD)hCtx;
  for(int i=0;i<5;i++)
  {
    if(g_PktDataStore[i*2]==dwhCtx)
    {
      return g_PktDataStore[i*2+1];
    }
  }
  return 0;
}

HCTX WINAPI WTOpenA(HWND hWnd, LPVOID lpLogCtx, BOOL fEnable)
{
  char *buf = (char *)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 1024);
  LPLOGCONTEXTA lpCon = (LPLOGCONTEXTA)lpLogCtx;
  if(lstrcmpA(lpCon->lcName, "Azel WinTab")==0)
  {
  sprintf(buf, "WTOpenA: Azel WinTab: lpCon->lcInExtX = %d->%d, lpCon->lcInExtY = %d->%d\n", lpCon->lcInExtX, lpCon->lcOutExtX, lpCon->lcInExtY, lpCon->lcOutExtY);
  MyOutputDebugStringA(buf);
    lpCon->lcOutExtX=lpCon->lcInExtX;
    lpCon->lcOutExtY=lpCon->lcInExtY;
  }
  HCTX ret = p_WTOpenA(hWnd, lpLogCtx, fEnable);
  SetPktDataStore(ret, lpCon->lcPktData);
  unsigned char *pL = (unsigned char *)lpLogCtx;
  sprintf(buf, "WTOpenA: hWnd=0x%08X, lpLogCtx=0x%08X, fEnable=%s, ret=%s\n", hWnd, lpLogCtx, fEnable?"TRUE":"FALSE", ret?"Success":"Fail");
  MyOutputDebugStringA(buf);
  sprintf(buf, "   lcName=%s\n   lcOptions=%u\n   lcStatus=%u\n   lcLocks=%u\n   lcMsgBase=%u\n   lcDevice=%u\n   lcPktRate=%u\n   lcPktData=0x%08X\n   lcPktMode=0x%08X\n   lcMoveMask=0x%08X\n   lcBtnDnMask=0x%08X\n   lcBtnUpMask=0x%08X\n   lcInOrgX=%d\n   lcInOrgY=%d\n   lcInOrgZ=%d\n   lcInExtX=%d\n   lcInExtY=%d\n   lcInExtZ=%d\n   lcOutOrgX=%d\n   lcOutOrgY=%d\n   lcOutOrgZ=%d\n   lcOutExtX=%d\n   lcOutExtY=%d\n   lcOutExtZ=%d\n   lcSensX=%f\n   lcSensY=%f\n   lcSensZ=%f\n   lcSysMode=%s\n   lcSysOrgX=%d\n   lcSysOrgY=%d\n   lcSysExtX=%d\n   lcSysExtY=%d\n   lcSysSensX=%f\n   lcSysSensY=%f\n", lpCon->lcName, lpCon->lcOptions, lpCon->lcStatus, lpCon->lcLocks, lpCon->lcMsgBase, lpCon->lcDevice, lpCon->lcPktRate, lpCon->lcPktData, lpCon->lcPktMode, lpCon->lcMoveMask, lpCon->lcBtnDnMask, lpCon->lcBtnUpMask, lpCon->lcInOrgX, lpCon->lcInOrgY, lpCon->lcInOrgZ, lpCon->lcInExtX, lpCon->lcInExtY, lpCon->lcInExtZ, lpCon->lcOutOrgX, lpCon->lcOutOrgY, lpCon->lcOutOrgZ, lpCon->lcOutExtX, lpCon->lcOutExtY, lpCon->lcOutExtZ, FIX_DOUBLE(lpCon->lcSensX), FIX_DOUBLE(lpCon->lcSensY), FIX_DOUBLE(lpCon->lcSensZ), (lpCon->lcSysMode)?"TRUE":"FALSE", lpCon->lcSysOrgX, lpCon->lcSysOrgY, lpCon->lcSysExtX, lpCon->lcSysExtY, FIX_DOUBLE(lpCon->lcSysSensX), FIX_DOUBLE(lpCon->lcSysSensY));
  MyOutputDebugStringA(buf);
  HeapFree(GetProcessHeap(), 0, buf);
  return ret;
}

//__declspec( naked ) void d_WTClose() { _asm{ jmp p_WTClose } }

BOOL WINAPI WTClose(HCTX hCtx)
{
  BOOL ret = p_WTClose(hCtx);
  ClosePktDataStore(hCtx);
  char *buf = (char *)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 1024);
  sprintf(buf, "WTClose: hCtx=0x%08X, ret=%s\n", hCtx, ret?"Success":"Fail");
  MyOutputDebugStringA(buf);
  HeapFree(GetProcessHeap(), 0, buf);
  return ret;
}

//__declspec( naked ) void d_WTPacketsGet() { _asm{ jmp p_WTPacketsGet } }
int WINAPI WTPacketsGet(HCTX hCtx, int cMaxPkts, LPVOID lpPkts)
{
  if(hCtx==NULL)
  {
    //MyOutputDebugStringA("WTPacketsGet: HCTX hCtx==NULL\n");
    return 0;
  }
  int ret = p_WTPacketsGet(hCtx, cMaxPkts, lpPkts);
  char *buf = (char *)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 1024);
  sprintf(buf, "WTPacketsGet: HCTX hCtx=0x%08X, MaxPkts=%d, lpPkts=0x%08X, ret=%d\n", hCtx, cMaxPkts, lpPkts, ret);
  MyOutputDebugStringA(buf);
  HeapFree(GetProcessHeap(), 0, buf);
  return ret;
}


//__declspec( naked ) void d_WTPacket() { _asm{ jmp p_WTPacket } }

BOOL WINAPI WTPacket(HCTX hCtx, UINT wSerial, LPVOID lpPkt)
{
  BOOL ret = p_WTPacket(hCtx, wSerial, lpPkt);
  char *buf = (char *)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 1024);
  char *buf2 = (char *)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 1024);
  DWORD *dw = (DWORD*)lpPkt;
  //sprintf(buf, "WTPacket: wSerial=%u, lpPkt=0x%08X, p0=0x%08X, p1=0x%08X, p2=0x%08X, p3=0x%08X, p4=0x%08X, g_lcPktData=0x%08X, ret=%s\n", wSerial, lpPkt, *dw, *(dw+1), *(dw+2), *(dw+3), *(dw+4), g_lcPktData, ret?"Success":"Fail");
  sprintf(buf, "WTPacket: wSerial=%u, lpPkt=0x%08X, ", wSerial, lpPkt);
  DWORD *dw2 = dw;
  DWORD lcPktData = GetPktDataStore(hCtx);
  if(lcPktData & PK_CONTEXT)
  {
    sprintf(buf2, "pkContext=0x%08X, ", *dw2);
    lstrcatA(buf, buf2);
    dw2++;
  }
  if(lcPktData & PK_STATUS)
  {
    sprintf(buf2, "PK_STATUS=0x%08X, ", *dw2);
    lstrcatA(buf, buf2);
    dw2++;
  }
  if(lcPktData & PK_TIME)
  {
    sprintf(buf2, "PK_TIME=0x%08X, ", *dw2);
    lstrcatA(buf, buf2);
    dw2++;
  }
  if(lcPktData & PK_CHANGED)
  {
    sprintf(buf2, "PK_CHANGED=0x%08X, ", *dw2);
    lstrcatA(buf, buf2);
    dw2++;
  }
  if(lcPktData & PK_SERIAL_NUMBER)
  {
    sprintf(buf2, "PK_SERIAL_NUMBER=0x%08X, ", *dw2);
    lstrcatA(buf, buf2);
    dw2++;
  }
  if(lcPktData & PK_CURSOR)
  {
    sprintf(buf2, "PK_CURSOR=0x%08X, ", *dw2);
    lstrcatA(buf, buf2);
    dw2++;
  }
  if(lcPktData & PK_BUTTONS)
  {
#ifdef WINEPKBUTTUNSFIX
    if((*dw2) > 0)*dw2--;
#endif //WINEPKBUTTUNSFIX
    sprintf(buf2, "PK_BUTTONS=0x%08X, ", *dw2);
    lstrcatA(buf, buf2);
    dw2++;
  }
  if(lcPktData & PK_X)
  {
    sprintf(buf2, "PK_X=0x%08X, ", *dw2);
    lstrcatA(buf, buf2);
    dw2++;
  }
  if(lcPktData & PK_Y)
  {
    sprintf(buf2, "PK_Y=0x%08X, ", *dw2);
    lstrcatA(buf, buf2);
    dw2++;
  }
  if(lcPktData & PK_Z)
  {
    sprintf(buf2, "PK_Z=0x%08X, ", *dw2);
    lstrcatA(buf, buf2);
    dw2++;
  }
  if(lcPktData & PK_NORMAL_PRESSURE)
  {
    sprintf(buf2, "PK_NORMAL_PRESSURE=0x%08X, ", *dw2);
    lstrcatA(buf, buf2);
    dw2++;
  }
  if(lcPktData & PK_TANGENT_PRESSURE)
  {
    sprintf(buf2, "PK_TANGENT_PRESSURE=0x%08X, ", *dw2);
    lstrcatA(buf, buf2);
    dw2++;
  }
  sprintf(buf2, "lcPktData=0x%08X, ret=%s\n", lcPktData, ret?"Success":"Fail");
  lstrcatA(buf, buf2);
  MyOutputDebugStringA(buf);
  HeapFree(GetProcessHeap(), 0, buf);
  HeapFree(GetProcessHeap(), 0, buf2);
  return ret;
}

__declspec( naked ) void d_WTEnable() { _asm{ jmp p_WTEnable } }
//__declspec( naked ) void d_WTOverlap() { _asm{ jmp p_WTOverlap } }

BOOL WINAPI WTOverlap(HCTX hCtx, BOOL fToTop)
{
  BOOL ret = p_WTOverlap(hCtx, fToTop);
  char *buf = (char *)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 1024);
  sprintf(buf, "WTOverlap: %s, ret=%s\n", fToTop?"TRUE":"FALSE", ret?"Success":"Fail");
  MyOutputDebugStringA(buf);
  HeapFree(GetProcessHeap(), 0, buf);
  return ret;
}

__declspec( naked ) void d_WTConfig() { _asm{ jmp p_WTConfig } }

//__declspec( naked ) void d_WTGetA() { _asm{ jmp p_WTGetA } }
BOOL WINAPI WTGetA(HCTX hCtx, LPVOID lpLogCtx)
{
  BOOL ret = 0;
  if(hCtx!=NULL && lpLogCtx!=NULL)ret = p_WTGetA(hCtx, lpLogCtx);
  return ret;
}

__declspec( naked ) void d_WTSetA() { _asm{ jmp p_WTSetA } }
__declspec( naked ) void d_WTExtGet() { _asm{ jmp p_WTExtGet } }
__declspec( naked ) void d_WTExtSet() { _asm{ jmp p_WTExtSet } }
__declspec( naked ) void d_WTSave() { _asm{ jmp p_WTSave } }
__declspec( naked ) void d_WTRestore() { _asm{ jmp p_WTRestore } }
//__declspec( naked ) void d_WTPacketsPeek() { _asm{ jmp p_WTPacketsPeek } }
__declspec( naked ) void d_WTDataGet() { _asm{ jmp p_WTDataGet } }
__declspec( naked ) void d_WTDataPeek() { _asm{ jmp p_WTDataPeek } }

//__declspec( naked ) void d_WTQueueSizeGet() { _asm{ jmp p_WTQueueSizeGet } }
int WINAPI WTQueueSizeGet(HCTX hCtx)
{
  int ret = p_WTQueueSizeGet(hCtx);
  char *buf = (char *)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 1024);
  sprintf(buf, "WTQueueSizeGet: ret=%d, from=%p\n", ret, _ReturnAddress());
  MyOutputDebugStringA(buf);
  HeapFree(GetProcessHeap(), 0, buf);
  return ret;
}

//__declspec( naked ) void d_WTQueueSizeSet() { _asm{ jmp p_WTQueueSizeSet } }

BOOL WINAPI WTQueueSizeSet(HCTX hCtx, int nPkts)
{
  BOOL ret = p_WTQueueSizeSet(hCtx, nPkts);
  char *buf = (char *)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 1024);
  sprintf(buf, "WTQueueSizeSet: nPkts=%i, ret=%s\n", nPkts, ret?"Success":"Fail");
  MyOutputDebugStringA(buf);
  HeapFree(GetProcessHeap(), 0, buf);
  return ret;
}

__declspec( naked ) void d_WTMgrOpen() { _asm{ jmp p_WTMgrOpen } }
__declspec( naked ) void d_WTMgrClose() { _asm{ jmp p_WTMgrClose } }
__declspec( naked ) void d_WTMgrContextEnum() { _asm{ jmp p_WTMgrContextEnum } }
__declspec( naked ) void d_WTMgrContextOwner() { _asm{ jmp p_WTMgrContextOwner } }
__declspec( naked ) void d_WTMgrDefContext() { _asm{ jmp p_WTMgrDefContext } }
__declspec( naked ) void d_WTMgrDeviceConfig() { _asm{ jmp p_WTMgrDeviceConfig } }
__declspec( naked ) void d_WTMgrExt() { _asm{ jmp p_WTMgrExt } }
__declspec( naked ) void d_WTMgrCsrEnable() { _asm{ jmp p_WTMgrCsrEnable } }
__declspec( naked ) void d_WTMgrCsrButtonMap() { _asm{ jmp p_WTMgrCsrButtonMap } }
__declspec( naked ) void d_WTMgrCsrPressureBtnMarks() { _asm{ jmp p_WTMgrCsrPressureBtnMarks } }
__declspec( naked ) void d_WTMgrCsrPressureResponse() { _asm{ jmp p_WTMgrCsrPressureResponse } }
__declspec( naked ) void d_WTMgrCsrExt() { _asm{ jmp p_WTMgrCsrExt } }
__declspec( naked ) void d_WTQueuePacketsEx() { _asm{ jmp p_WTQueuePacketsEx } }
__declspec( naked ) void d_WTMgrCsrPressureBtnMarksEx() { _asm{ jmp p_WTMgrCsrPressureBtnMarksEx } }
__declspec( naked ) void d_WTMgrConfigReplaceExA() { _asm{ jmp p_WTMgrConfigReplaceExA } }
__declspec( naked ) void d_WTMgrPacketHookExA() { _asm{ jmp p_WTMgrPacketHookExA } }
__declspec( naked ) void d_WTMgrPacketUnhook() { _asm{ jmp p_WTMgrPacketUnhook } }
__declspec( naked ) void d_WTMgrPacketHookNext() { _asm{ jmp p_WTMgrPacketHookNext } }
__declspec( naked ) void d_WTMgrDefContextEx() { _asm{ jmp p_WTMgrDefContextEx } }

//__declspec( naked ) void d_WTInfoW() { _asm{ jmp p_WTInfoW } }
UINT WINAPI WTInfoW(UINT wCategory, UINT nIndex, LPVOID lpOutput)
{
  BOOL ret = p_WTInfoW(wCategory, nIndex, lpOutput);
  char *buf = (char *)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 1024);
  sprintf(buf, "WTInfoW: wCategory=%u, nIndex=%u, lpOutput=0x%08X, ret=%u\n", wCategory, nIndex, lpOutput, ret);
  MyOutputDebugStringA(buf);
  
  if(ret)
  {
  if((wCategory==3/*WTI_DEFCONTEXT*/ || wCategory==4/*WTI_DEFSYSCTX*/) && nIndex==0)
  {
    MyOutputDebugStringA((wCategory==3)?"   cat3 WTI_DEFCONTEXT (direct - not cursor)":"   cat4 WTI_DEFSYSCTX (with cursor)");
  LPLOGCONTEXTW lpCon = (LPLOGCONTEXTW)lpOutput;
  sprintf(buf, "WTInfoW: REPLACE: lpCon->lcInExtX=%d -> %d, lpCon->lcInExtY=%d -> %d\n", lpCon->lcInExtX, lpCon->lcOutExtX, lpCon->lcInExtY, lpCon->lcOutExtY);
  MyOutputDebugStringA(buf);
    lpCon->lcOutExtX=lpCon->lcInExtX;
    lpCon->lcOutExtY=lpCon->lcInExtY;
  sprintf(buf, "   lcName=%s\n   lcOptions=%u\n   lcStatus=%u\n   lcLocks=%u\n   lcMsgBase=%u\n   lcDevice=%u\n   lcPktRate=%u\n   lcPktData=0x%08X\n   lcPktMode=0x%08X\n   lcMoveMask=0x%08X\n   lcBtnDnMask=0x%08X\n   lcBtnUpMask=0x%08X\n   lcInOrgX=%d\n   lcInOrgY=%d\n   lcInOrgZ=%d\n   lcInExtX=%d\n   lcInExtY=%d\n   lcInExtZ=%d\n   lcOutOrgX=%d\n   lcOutOrgY=%d\n   lcOutOrgZ=%d\n   lcOutExtX=%d\n   lcOutExtY=%d\n   lcOutExtZ=%d\n   lcSensX=%f\n   lcSensY=%f\n   lcSensZ=%f\n   lcSysMode=%s\n   lcSysOrgX=%d\n   lcSysOrgY=%d\n   lcSysExtX=%d\n   lcSysExtY=%d\n   lcSysSensX=%f\n   lcSysSensY=%f\n", lpCon->lcName, lpCon->lcOptions, lpCon->lcStatus, lpCon->lcLocks, lpCon->lcMsgBase, lpCon->lcDevice, lpCon->lcPktRate, lpCon->lcPktData, lpCon->lcPktMode, lpCon->lcMoveMask, lpCon->lcBtnDnMask, lpCon->lcBtnUpMask, lpCon->lcInOrgX, lpCon->lcInOrgY, lpCon->lcInOrgZ, lpCon->lcInExtX, lpCon->lcInExtY, lpCon->lcInExtZ, lpCon->lcOutOrgX, lpCon->lcOutOrgY, lpCon->lcOutOrgZ, lpCon->lcOutExtX, lpCon->lcOutExtY, lpCon->lcOutExtZ, FIX_DOUBLE(lpCon->lcSensX), FIX_DOUBLE(lpCon->lcSensY), FIX_DOUBLE(lpCon->lcSensZ), (lpCon->lcSysMode)?"TRUE":"FALSE", lpCon->lcSysOrgX, lpCon->lcSysOrgY, lpCon->lcSysExtX, lpCon->lcSysExtY, FIX_DOUBLE(lpCon->lcSysSensX), FIX_DOUBLE(lpCon->lcSysSensY));
  MyOutputDebugStringA(buf);
  }
  }
  HeapFree(GetProcessHeap(), 0, buf);
  return ret;
}

//__declspec( naked ) void d_WTOpenW() { _asm{ jmp p_WTOpenW } }
HCTX WINAPI WTOpenW(HWND hWnd, LPVOID lpLogCtx, BOOL fEnable)
{
  LPLOGCONTEXTW lpCon = (LPLOGCONTEXTW)lpLogCtx;
  char *buf = (char *)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 1024);
//  if(lstrcmpW(lpCon->lcName, L"Azel WinTab")==0)
  {
  sprintf(buf, "WTOpenW: Azel WinTab: lpCon->lcInExtX=%d -> %d, lpCon->lcInExtY=%d -> %d\n", lpCon->lcInExtX, lpCon->lcOutExtX, lpCon->lcInExtY, lpCon->lcOutExtY);
  MyOutputDebugStringA(buf);
    lpCon->lcOutExtX=lpCon->lcInExtX;
    lpCon->lcOutExtY=lpCon->lcInExtY;
  }
  HCTX ret = p_WTOpenW(hWnd, lpLogCtx, fEnable);
  unsigned char *pL = (unsigned char *)lpLogCtx;
  sprintf(buf, "WTOpenW: hWnd=0x%08X, lpLogCtx=0x%08X, fEnable=%s, lcPktData=0x%08X, lcPktMode=0x%08X, ret=%s\n", hWnd, lpLogCtx, fEnable?"TRUE":"FALSE", *((DWORD*)(pL+64)), *((DWORD*)(pL+68)), ret?"Success":"Fail");
  MyOutputDebugStringA(buf);
  sprintf(buf, "   lcName=%s\n   lcOptions=%u\n   lcStatus=%u\n   lcLocks=%u\n   lcMsgBase=%u\n   lcDevice=%u\n   lcPktRate=%u\n   lcPktData=0x%08X\n   lcPktMode=0x%08X\n   lcMoveMask=0x%08X\n   lcBtnDnMask=0x%08X\n   lcBtnUpMask=0x%08X\n   lcInOrgX=%d\n   lcInOrgY=%d\n   lcInOrgZ=%d\n   lcInExtX=%d\n   lcInExtY=%d\n   lcInExtZ=%d\n   lcOutOrgX=%d\n   lcOutOrgY=%d\n   lcOutOrgZ=%d\n   lcOutExtX=%d\n   lcOutExtY=%d\n   lcOutExtZ=%d\n   lcSensX=%f\n   lcSensY=%f\n   lcSensZ=%f\n   lcSysMode=%s\n   lcSysOrgX=%d\n   lcSysOrgY=%d\n   lcSysExtX=%d\n   lcSysExtY=%d\n   lcSysSensX=%f\n   lcSysSensY=%f\n", lpCon->lcName, lpCon->lcOptions, lpCon->lcStatus, lpCon->lcLocks, lpCon->lcMsgBase, lpCon->lcDevice, lpCon->lcPktRate, lpCon->lcPktData, lpCon->lcPktMode, lpCon->lcMoveMask, lpCon->lcBtnDnMask, lpCon->lcBtnUpMask, lpCon->lcInOrgX, lpCon->lcInOrgY, lpCon->lcInOrgZ, lpCon->lcInExtX, lpCon->lcInExtY, lpCon->lcInExtZ, lpCon->lcOutOrgX, lpCon->lcOutOrgY, lpCon->lcOutOrgZ, lpCon->lcOutExtX, lpCon->lcOutExtY, lpCon->lcOutExtZ, FIX_DOUBLE(lpCon->lcSensX), FIX_DOUBLE(lpCon->lcSensY), FIX_DOUBLE(lpCon->lcSensZ), (lpCon->lcSysMode)?"TRUE":"FALSE", lpCon->lcSysOrgX, lpCon->lcSysOrgY, lpCon->lcSysExtX, lpCon->lcSysExtY, FIX_DOUBLE(lpCon->lcSysSensX), FIX_DOUBLE(lpCon->lcSysSensY));
  MyOutputDebugStringA(buf);
  HeapFree(GetProcessHeap(), 0, buf);
  return ret;
}

//__declspec( naked ) void d_WTGetW() { _asm{ jmp p_WTGetW } }
BOOL WINAPI WTGetW(HCTX hCtx, LPLOGCONTEXTW lpLogCtx)
{
  LPLOGCONTEXTW lpCon = (LPLOGCONTEXTW)lpLogCtx;
  BOOL ret = p_WTGetW(hCtx, lpLogCtx);
  char *buf = (char *)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 1024);
  unsigned char *pL = (unsigned char *)lpLogCtx;
  sprintf(buf, "WTGetW: lpLogCtx=0x%08X, ret=%s\n", lpLogCtx, ret?"Success":"Fail");
  MyOutputDebugStringA(buf);
  sprintf(buf, "   lcName=%s\n   lcOptions=%u\n   lcStatus=%u\n   lcLocks=%u\n   lcMsgBase=%u\n   lcDevice=%u\n   lcPktRate=%u\n   lcPktData=0x%08X\n   lcPktMode=0x%08X\n   lcMoveMask=0x%08X\n   lcBtnDnMask=0x%08X\n   lcBtnUpMask=0x%08X\n   lcInOrgX=%d\n   lcInOrgY=%d\n   lcInOrgZ=%d\n   lcInExtX=%d\n   lcInExtY=%d\n   lcInExtZ=%d\n   lcOutOrgX=%d\n   lcOutOrgY=%d\n   lcOutOrgZ=%d\n   lcOutExtX=%d\n   lcOutExtY=%d\n   lcOutExtZ=%d\n   lcSensX=%u\n   lcSensY=%u\n   lcSensZ=%u\n   lcSysMode=%s\n   lcSysOrgX=%d\n   lcSysOrgY=%d\n   lcSysExtX=%d\n   lcSysExtY=%d\n   lcSysSensX=%u\n   lcSysSensY=%u\n", lpCon->lcName, lpCon->lcOptions, lpCon->lcStatus, lpCon->lcLocks, lpCon->lcMsgBase, lpCon->lcDevice, lpCon->lcPktRate, lpCon->lcPktData, lpCon->lcPktMode, lpCon->lcMoveMask, lpCon->lcBtnDnMask, lpCon->lcBtnUpMask, lpCon->lcInOrgX, lpCon->lcInOrgY, lpCon->lcInOrgZ, lpCon->lcInExtX, lpCon->lcInExtY, lpCon->lcInExtZ, lpCon->lcOutOrgX, lpCon->lcOutOrgY, lpCon->lcOutOrgZ, lpCon->lcOutExtX, lpCon->lcOutExtY, lpCon->lcOutExtZ, (lpCon->lcSensX), (lpCon->lcSensY), (lpCon->lcSensZ), (lpCon->lcSysMode)?"TRUE":"FALSE", lpCon->lcSysOrgX, lpCon->lcSysOrgY, lpCon->lcSysExtX, lpCon->lcSysExtY, (lpCon->lcSysSensX), (lpCon->lcSysSensY));
  MyOutputDebugStringA(buf);
  HeapFree(GetProcessHeap(), 0, buf);
  {
  LPLOGCONTEXTW lpCon = (LPLOGCONTEXTW)lpLogCtx;
  sprintf(buf, "WTGetW: REPLACE: lpCon->lcInExtX=%d -> %d, lpCon->lcInExtY=%d -> %d\n", lpCon->lcInExtX, lpCon->lcOutExtX, lpCon->lcInExtY, lpCon->lcOutExtY);
  MyOutputDebugStringA(buf);
    lpCon->lcOutExtX=lpCon->lcInExtX;
    lpCon->lcOutExtY=lpCon->lcInExtY;
  }
  return ret;
}


__declspec( naked ) void d_WTSetW() { _asm{ jmp p_WTSetW } }
__declspec( naked ) void d_WTMgrConfigReplaceExW() { _asm{ jmp p_WTMgrConfigReplaceExW } }
__declspec( naked ) void d_WTMgrPacketHookExW() { _asm{ jmp p_WTMgrPacketHookExW } }


int WINAPI WTPacketsPeek(HCTX hCtx, int cMaxPkts, LPVOID lpPkts)
{
  if(hCtx==NULL || lpPkts==NULL)return 0;
  int ret = p_WTPacketsPeek(hCtx, cMaxPkts, lpPkts);
  char *buf = (char *)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 1024);
  sprintf(buf, "WTPacketsPeek: cMaxPkts=%i, lpPkts=0x%08X, ret=%d, from=%p\n", cMaxPkts, lpPkts, ret, _ReturnAddress());
  MyOutputDebugStringA(buf);
  HeapFree(GetProcessHeap(), 0, buf);
  return ret;
}
